from sqlalchemy import *
from sqlalchemy.orm import relationship
from Models.Model import *


class Utilisateurs(Model):
    __tablename__ = 'Utilisateurs'
    id = Column(String, primary_key=True, nullable=False,
                autoincrement=True)
    login = Column(String(200), nullable=False)
    password = Column(String(30), nullable=True)
    nom = Column(String(200), nullable=False)
    prenom = Column(String(200), nullable=False)
    telephone = Column(Integer, nullable=False)
    email = Column(String(200), nullable=False)
    adresse = Column(String(200), nullable=False)
    actif = Column(Boolean, nullable=False)
    service = relationship("Service")

    def __init__(self, login, password, nom, prenom, telephone, email, adresse, actif, service):
        self.login = login
        self.password = password
        self.nom = nom
        self.prenom = prenom
        self.telephone = telephone
        self.email = email
        self.adresse = adresse
        self.actif = actif
        self.service = service


