from sqlalchemy import *
from sqlalchemy.orm import relationship
from Models.Model import *

class Chambre(Model):
    __tablename__ = 'Chambre'
    id = Column(String, primary_key=True, nullable=False,
                autoincrement=True)
    prix = Column(Integer, nullable=False)
    categorie = relationship("Categorie")