from sqlalchemy.orm import relationship
from Models.Model import *

class Hotel(Model):
    __tablename__ = 'Hotel'
    utilisateurs = relationship("Utilisateurs")
    chambres = relationship("Chambres")
    commandes = relationship("Commandes")
    clients = relationship("Clients")
    service = relationship("Service")