from sqlalchemy import *
from sqlalchemy.orm import relationship
from Models.Model import *

class Sejour(Model):
    __tablename__ = 'Sejour'
    id = Column(String, primary_key=True, nullable=False,
                autoincrement=True)
    date_arrivee = Column(Date, nullable=False)
    date_depart = Column(Date, nullable=False)
    utilisateur = relationship("Utilisateur")
    nom_client = Column(Integer, ForeignKey('Client.nom'))
    nom_chambre = Column(Integer, ForeignKey('Chambre.nom'))