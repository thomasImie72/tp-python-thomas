from sqlalchemy import *
from sqlalchemy.orm import relationship
from Models.Model import *

class Commandes(Model):
    __tablename__ = 'Commandes'
    id = Column(String, primary_key=True, nullable=False,
                autoincrement=True)
    date = Column(Date, nullable=True)
    client = relationship("Client")
    produit = relationship("Produit")