from sqlalchemy import *
from sqlalchemy.orm import relationship
from Models.Model import *

class Client(Model):
    __tablename__ = 'Client'
    id = Column(String, primary_key=True, nullable=False,
                autoincrement=True)
    nom = Column(String(200), nullable=False)
    prenom = Column(String(200), nullable=False)
    numero_telephone = Column(Integer, nullable=False)
    numero_carte_bancaire = Column(Integer, nullable=False)
    adresse = Column(String(200), nullable=False)
    sejours = Column(Integer, nullable=False)