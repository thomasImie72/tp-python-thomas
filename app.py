from flask import Flask
from data import *

app = Flask(__name__)


@app.route('/')
def index():
    return "Hello !"


@app.route('/utilisateurs')
def get_users():
    return get_utilisateurs()


@app.route('/commandes/<Date:dateDebut>/<Date:dateFin>')
def get_commandes():
    # TODO


@app.route('/sejours/<Date:dateDebut>/<Date:dateFin>')
def get_sejours():
    # TODO


if __name__ == '__main__':
    app.run(debug=True)
