from Models import Utilisateurs
from Models.Service import Service

utilisateurs = {
    Utilisateurs(
        'userLogin',
        'password',
        'Thomas',
        'Lemaitre',
        '0606060606',
        'myemail@yahoo.com',
        'my address',
        1,
        Service.BAR
    ),
    Utilisateurs(
        'userLogin2',
        'password2',
        'Val',
        'André',
        '0707070707',
        'myemail@yahoo.com',
        'my address 2',
        0,
        Service.COMPTABILITE
    )
}


def get_utilisateurs():
    return utilisateurs
